#include "mbed.h"
#include <cmath>
AnalogIn in(PC_0);


#define BUF_LEN 80000
#define GPS_PPS_PIN PA_3

Serial pc(USBTX,USBRX,921600);
DigitalOut led(LED1);
DigitalOut led2(LED2);

InterruptIn gps_pps(GPS_PPS_PIN);
InterruptIn user_button(USER_BUTTON);


//float buffer[BUF_LEN];
uint16_t buffer_u16[BUF_LEN];
CircularBuffer<uint16_t,10000> cbuf;
uint16_t tmp; 

Timer tim;
Timer delayFromSec; 
bool newbuffer = false;
bool first = false;

float trigger = 0.01f;
float offset = 0.665f;

void resetTime() {
    delayFromSec.reset();
    delayFromSec.start();
    led2 = !led2;
}

int main()
{
    gps_pps.rise(&resetTime);
    user_button.rise(&resetTime);

    pc.printf("\nAnalog loop example \r\n");
    pc.printf("*** Connect A0 and PA_4 pins together ***\r\n");
    while(1) {

        while ( abs(in.read()-offset) < trigger) {};
        /*while(true) {
            tmp = in.read_u16();
            cbuf.push(tmp);
            if (abs(tmp-offset) < trigger) break;
        }*/
        
        delayFromSec.stop();
        float timedelay = delayFromSec.read();
        tim.reset();
        tim.start();
        for (int i = 0; i < BUF_LEN; i++) {
            // Read ADC input
            //buffer[i] = in.read();
            buffer_u16[i] = in.read_u16();

     
        }
        tim.stop();
       
            pc.printf("NB with %10.9f at delay: %10.9f \n\r",tim.read(), 
                       timedelay);
            while(cbuf.pop(tmp)) {
                pc.printf("%u\n\r",tmp);
            }
            for (int j=0; j<BUF_LEN; j++) {
                //pc.printf("%8.3f\n\r",buffer[j]);
                pc.printf("%u\n\r",buffer_u16[j]);

                buffer_u16[j] = 0;
            }
            
            
        
    }
}

